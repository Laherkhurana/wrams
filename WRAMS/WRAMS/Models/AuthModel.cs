﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WRAMS.Models
{
    public class AuthModel
    {
        public Status Status { get; set; }
        public List<UserList> UserList { get; set; }
        public List<CorpBranchLatitudeList> CorpBranchLatitudeList { get; set; }
        public AuthModel()
        {
            Status = new Status();
            UserList = new List<UserList>();
            CorpBranchLatitudeList = new List<CorpBranchLatitudeList>();
        }
    }
    public class Status
    {
        public string MessageCode { get; set; }
        public string Message { get; set; }
    }

    public class UserList
    {
        public int UserId { get; set; }
        public string MWU_FirstName { get; set; }
        public string MWU_EmailId { get; set; }
        public string MWU_UserName { get; set; }
        public string MWU_MobileNo { get; set; }
        public string MWU_Gender { get; set; }
        public string MWU_MiddleName { get; set; }
        public string MWU_LastName { get; set; }
        public string vchGroupName { get; set; }
        public int GroupId { get; set; }
        public string FullName { get; set; }
    }

    public class CorpBranchLatitudeList
    {
        public int DeploymentId { get; set; }
        public int CorporateId { get; set; }
        public int BranchId { get; set; }
        public int EntityId { get; set; }
        public string vchLatitude { get; set; }
        public string vchLongitude { get; set; }
    }

}
