﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WRAMS.Interfaces
{
    public interface IFtpWebRequest
    {
        string upload(string FtpUrl, string fileName, string path_, string userName, string password, string UploadDirectory = "");
    }
}
