﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WRAMS.Interfaces
{
    public interface IToastNotifier
    {
        void Notify(ToastNotificationType type, string title, string description, TimeSpan duration, object context = null);

        void HideAll();
    }
    public enum ToastNotificationType
    {
        Info,
        Success,
        Error,
        Warning,
    }
}