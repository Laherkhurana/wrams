﻿using WRAMS.ViewModels;
using Xamarin.Forms;

namespace WRAMS.Views
{
    public partial class AttendancePage : ContentPage
    {
        public AttendancePage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            var vm = BindingContext as AttendancePageViewModel;
            vm.Logout();
            return true;
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            var vm = BindingContext as AttendancePageViewModel;
            vm.Logout();
        }
    }
}
