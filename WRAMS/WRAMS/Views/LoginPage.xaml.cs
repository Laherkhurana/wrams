﻿using System;
using WRAMS.Constants;
using Xamarin.Forms;

namespace WRAMS.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            entryUserName.Focus();
            entryPassword.IsPassword = true;
            entryPassword.WidthRequest = AppConstants.ScreenWidth / 1.43;
            entryUserName.Completed += (object sender, EventArgs e) => this.entryPassword.Focus();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            entryPassword.IsPassword = !entryPassword.IsPassword;
            labelshow.Text = (entryPassword.IsPassword) ? "SHOW" : "HIDE";
        }
    }
}
