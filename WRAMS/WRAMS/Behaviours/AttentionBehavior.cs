﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WRAMS.Behaviours
{
    public class AttentionBehavior : Behavior<View>
    {
        public static readonly BindableProperty ShouldSeekProperty = BindableProperty.Create("ShouldSeek", typeof(bool), typeof(AttentionBehavior), false, BindingMode.TwoWay, null, OnBindableShouldSeekChanged);

        View current;
        public bool ShouldSeek
        {
            get { return (bool)GetValue(ShouldSeekProperty); }
            set { SetValue(ShouldSeekProperty, (bool)value); }
        }

        protected override void OnAttachedTo(View bindable)
        {
            current = bindable;
        }

        static void OnBindableShouldSeekChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as AttentionBehavior).OnShouldSeekChanged((bool)oldValue, (bool)newValue);
        }

        async void OnShouldSeekChanged(bool oldValue, bool newValue)
        {
            if (newValue == true)
            {
                await Seek();
            }
        }

        async Task Seek()
        {
            await current.ScaleTo(1.35, 250, Easing.SinIn);
            await current.ScaleTo(1, 250, Easing.SinIn);
            ShouldSeek = false;
        }
    }
}   