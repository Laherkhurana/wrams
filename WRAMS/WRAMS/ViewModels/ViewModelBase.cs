﻿using Plugin.Connectivity;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WRAMS.Interfaces;

namespace WRAMS.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        protected INavigationService NavigationService { get; set; }
        protected IEventAggregator ea;
        protected IToastNotifier notifier;
        protected IRestClient restClient;
        protected IPageDialogService dialogService;
       
        public List<Task> TaskList = new List<Task>();

        public DelegateCommand<string> NavigateCommand { get; set; }

        private bool showError = false;
        public bool ShowError
        {
            get { return showError; }
            set { SetProperty(ref showError, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool updating;
        public bool Updating
        {
            get { return updating; }
            set { SetProperty(ref updating, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }
        private bool isEnabled = true;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set { SetProperty(ref isEnabled, value); }
        }
        private bool isVisible = true;
        public bool IsVisible
        {
            get { return isVisible; }
            set { SetProperty(ref isVisible, value); }
        }
        private bool isModal = false;
        public bool IsModal
        {
            get { return isModal; }
            set { SetProperty(ref isModal, value); }
        }

        string busyMessage = "";
        public string BusyMessage
        {
            get { return busyMessage; }
            set { SetProperty(ref busyMessage, value); }
        }

        string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }


        bool isConnected = true;
        public bool IsConnected
        {
            get { return isConnected; }
            set { SetProperty(ref isConnected, value); }
        }
        public bool IsNotConnected
        {
            get { return !IsConnected; }
        }

        public ViewModelBase()
        {
            NavigateCommand = new DelegateCommand<string>(async (string page) => await NavigateTo(page));
        }

        public ViewModelBase(INavigationService navigationService, IEventAggregator eventAggregator, IToastNotifier toastNotifier, IRestClient client, IPageDialogService pageDialogService)
        {
            NavigationService = navigationService;
            ea = eventAggregator;
            notifier = toastNotifier;
            restClient = client;
            dialogService = pageDialogService;
            NavigateCommand = new DelegateCommand<string>(async (string page) => await NavigateTo(page));
        }

        public virtual async Task NavigateTo(string pageName, NavigationParameters param = null)
        {
            await NavigationService.NavigateAsync(pageName, param, false);
        }

        protected virtual void ApplyLocaleStrings()
        {

        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public async Task GoToPreviousPage(NavigationParameters parameters = null, bool? useModalNavigation = null, bool animated = true)
        {
            // BusyMessageView messageView = new BusyMessageView();
            // await PopupNavigation.PushAsync(messageView);
            try
            {
                await NavigationService.GoBackAsync(parameters, useModalNavigation, animated);
            }
            catch (Exception) { }
            finally
            {
                //  await PopupNavigation.PopAsync();
            }

        }
        public async Task Navigate(Uri uri, NavigationParameters parameters = null, bool? useModalNavigation = null, bool animated = true)
        {
            //  BusyMessageView messageView = new BusyMessageView();
            // await PopupNavigation.PushAsync(messageView);
            try
            {
                await NavigationService.NavigateAsync(uri, parameters, useModalNavigation, animated);
            }
            catch (Exception) { }
            finally
            {
                // await PopupNavigation.PopAsync();
            }
        }
        public async Task Navigate(string name, NavigationParameters parameters = null, bool? useModalNavigation = null, bool animated = true)
        {
            // BusyMessageView messageView = new BusyMessageView();
            // await PopupNavigation.PushAsync(messageView);
            try
            {
                await NavigationService.NavigateAsync(name, parameters, useModalNavigation, animated);
            }
            catch (Exception) { }
            finally
            {
                //  await PopupNavigation.PopAsync();
            }
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
            IsConnected = CrossConnectivity.Current.IsConnected;
            RaisePropertyChanged("IsNotConnected");
        }

        public virtual void ConnectivityChanged(bool connected)
        {
            IsConnected = connected;
            RaisePropertyChanged("IsNotConnected");
        }

        public void NotifyPropertyChange(string propertyName = "")
        {
            if (propertyName != "" && propertyName.Trim().Length > 0)
                RaisePropertyChanged(propertyName);
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
            RaisePropertyChanged("IsNotConnected");
        }

        public virtual void Destroy()
        {

        }

      
    }
}