﻿using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WRAMS.Constants;
using WRAMS.Helpers;
using WRAMS.Interfaces;
using WRAMS.Models;

namespace WRAMS.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private string userName;
        public string UserName
        {
            get { return userName; }
            set { SetProperty(ref userName, value); }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private bool showError2 = false;
        public bool ShowError2
        {
            get { return showError2; }
            set { SetProperty(ref showError2, value); }
        }

        private List<string> pa = new List<string>();
        public List<string> Pa
        {
            get { return pa; }
            set { SetProperty(ref pa, value); }
        }
        // public List<string> pa = 
        Plugin.Geolocator.Abstractions.Position position;
        public DelegateCommand LoginCommand { get; set; }

        public LoginPageViewModel(INavigationService navigationService, IToastNotifier toastNotifier, IRestClient restclient, IPageDialogService pageDialogService)
        {
            NavigationService = navigationService;
            restClient = restclient;
            notifier = toastNotifier;
            dialogService = pageDialogService;
            LoginCommand = new DelegateCommand(async () => await OnLoginCommand(), () => { return IsEnabled; });
        }

        async Task OnLoginCommand()
        {
            try
            {
                IsEnabled = false;
                LoginCommand.RaiseCanExecuteChanged();
                if (!CrossConnectivity.Current.IsConnected)
                {
                    notifier.Notify(ToastNotificationType.Error, "", "No Connection", TimeSpan.FromMilliseconds(3000));
                    return;
                }
                if (string.IsNullOrEmpty(UserName))
                {
                    notifier.Notify(ToastNotificationType.Error, "", "Username can not be empty", TimeSpan.FromMilliseconds(3000));
                    ShowError = true;
                    return;
                }
                else if (string.IsNullOrEmpty(Password))
                {
                    notifier.Notify(ToastNotificationType.Error, "", "Password can not be empty", TimeSpan.FromMilliseconds(3000));
                    ShowError2 = true;
                    return;
                }
                else
                {
                    var output = await restClient.GetAsync<AuthModel>(AppConstants.WRAMS_BaseURl + AppConstants.AuthUserId + UserName + AppConstants.AuthPassword + Password, true);
                    if (output.Status.Message == "Success")
                    {
                        await GetCurrentLocation();
                        if (position == null)
                        {
                            notifier.Notify(ToastNotificationType.Error, "", "null cached location :(", TimeSpan.FromMilliseconds(3000));
                            return;
                        }
                        else
                        {
                            foreach (var item in output.CorpBranchLatitudeList)
                            {
                                string branchlat = item.vchLatitude.Substring(0, 6);
                                string branchlong = item.vchLongitude.Substring(0, 6);
                                string currentlat = position.Latitude.ToString().Substring(0, 6);
                                string currentlong = position.Longitude.ToString().Substring(0, 6);
                                if (branchlat == currentlat && branchlong == currentlong)
                                {
                                    NavigationParameters param = new NavigationParameters
                                    {
                                        { "model",  output},
                                    };
                                    NavigationService.NavigateAsync("AttendancePage", param, false);
                                    break;
                                }
                                else
                                    await dialogService.DisplayAlertAsync("Alert!", "Current location mismatched with alloted wellness room location, so you are not allowed to login in the system. Please reach to alloted wellness room location to mark the attendance.", "OK");
                            }
                        }
                    }
                    else
                    {
                        notifier.Notify(ToastNotificationType.Error, "", "Wrong Username or Password", TimeSpan.FromMilliseconds(3000));
                        ShowError = true;
                        ShowError2 = true;
                    }
                }
            }
            catch (Exception ex)
            {
                notifier.Notify(ToastNotificationType.Error, "", "Something went wrong, but don't worry we captured for analysis! Thanks.", TimeSpan.FromMilliseconds(3000));
                ex.ToString();
            }
            finally
            {
                IsEnabled = true;
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        async Task GetCurrentLocation()
        {
            var hasPermission = await Permissions.CheckPermissions(Permission.Location);
            if (!hasPermission)
                return;
            var locator = CrossGeolocator.Current;
            var position1 = await locator.GetPositionAsync(TimeSpan.FromSeconds(6000));
            position = new Plugin.Geolocator.Abstractions.Position(position1.Latitude, position1.Longitude);
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            var msz = CrossConnectivity.Current.IsConnected ? "Connected" : "No Connection";
            notifier.Notify(ToastNotificationType.Error, "",msz , TimeSpan.FromMilliseconds(3000));
        }
    }
}
