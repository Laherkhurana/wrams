﻿using Plugin.Connectivity;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WRAMS.Constants;
using WRAMS.Helpers;
using WRAMS.Interfaces;
using WRAMS.Models;
using Xamarin.Forms;

namespace WRAMS.ViewModels
{
    public class AttendancePageViewModel : ViewModelBase
    {
        private AuthModel model;
        public AuthModel Model
        {
            get { return model; }
            set { SetProperty(ref model, value); }
        }

        private string welcomeText;
        public string WelcomeText
        {
            get { return welcomeText; }
            set { SetProperty(ref welcomeText, value); }
        }

        private string currrentTime;
        public string CurrrentTime
        {
            get { return currrentTime; }
            set { SetProperty(ref currrentTime, value); }
        }

        private string currrentDate;
        public string CurrrentDate
        {
            get { return currrentDate; }
            set { SetProperty(ref currrentDate, value); }
        }

        private string dateType;
        public string DateType
        {
            get { return dateType; }
            set { SetProperty(ref dateType, value); }
        }

        private string timeType;
        public string TimeType
        {
            get { return timeType; }
            set { SetProperty(ref timeType, value); }
        }

        private ImageSource userImage;
        public ImageSource UserImage
        {
            get { return userImage; }
            set { SetProperty(ref userImage, value); }
        }

        private MediaFile _mediaFile;
        string fileName;
        byte[] bytes;
        IFtpWebRequest ftpWebRequest;
        public DelegateCommand CheckInCommand { get; set; }
        public DelegateCommand CheckOutCommand { get; set; }
        public AttendancePageViewModel(INavigationService navigationService, IFtpWebRequest ftp, IToastNotifier toastNotifier, IRestClient restclient, IPageDialogService pageDialogService)
        {
            NavigationService = navigationService;
            restClient = restclient;
            notifier = toastNotifier;
            dialogService = pageDialogService;
            ftpWebRequest = ftp;
            model = new AuthModel();
            CheckInCommand = new DelegateCommand(async () => await OnCheckIn(), () => { return IsEnabled; });
            CheckOutCommand = new DelegateCommand(async () => await OnCheckOut(), () => { return IsEnabled; });
        }

        async Task OnCheckOut()
        {
            try
            {
                IsEnabled = false;
                CheckOutCommand.RaiseCanExecuteChanged();
                if (!CrossConnectivity.Current.IsConnected)
                    notifier.Notify(ToastNotificationType.Error, "", "No Connection", TimeSpan.FromMilliseconds(3000));
                else
                    await TakePicture("Out Time", "Check-Out Time", "Check-Out Date");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                IsEnabled = true;
                CheckOutCommand.RaiseCanExecuteChanged();
            }
        }

        async Task OnCheckIn()
        {
            try
            {
                IsEnabled = false;
                CheckInCommand.RaiseCanExecuteChanged();
                if (!CrossConnectivity.Current.IsConnected)
                    notifier.Notify(ToastNotificationType.Error, "", "No Connection", TimeSpan.FromMilliseconds(3000));
                else
                    await TakePicture("In Time", "Check-In Time", "Check-In Date");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                IsEnabled = true;
                CheckInCommand.RaiseCanExecuteChanged();
            }
        }

        async Task TakePicture(string type, string timetype, string datetype)
        {
            try
            {
                bool valid = false;
                var content = new MultipartFormDataContent();
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }
                else
                    valid = true;
                if ((cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted) | valid)
                {
                    valid = true;
                    await CrossMedia.Current.Initialize();
                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        notifier.Notify(ToastNotificationType.Error, "", ":( No camera available.", TimeSpan.FromMilliseconds(2500));
                        return;
                    }
                    _mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "Sample",
                        Name = DateTime.Now.ToString("HH:mm:ss") + "_" + DateTime.Now.ToString("MM/dd/yyyy") + "Image.jpg",
                        DefaultCamera = CameraDevice.Front,
                        PhotoSize = PhotoSize.Medium,
                        CompressionQuality = 95,
                    });
                    if (_mediaFile == null)
                        return;
                    else
                    {
                        //var memoryStream = new MemoryStream();
                        string toBeSearched = "Sample/";
                        int ix = (_mediaFile.Path).IndexOf(toBeSearched);
                        if (ix != -1)
                        {
                            fileName = (_mediaFile.Path).Substring(ix + toBeSearched.Length);
                          //  _mediaFile.GetStream().CopyTo(memoryStream);
                            // _mediaFile.Dispose();
                            //  content.Add(new StreamContent(new MemoryStream(memoryStream.ToArray())), "photo", fileName);
                        }
                        var ftpResponse = ftpWebRequest.upload(AppConstants.WRAMS_FTP_URl + AppConstants.FTP_DirectoryPath, fileName, _mediaFile.Path, AppConstants.FTP_UserName, AppConstants.FTP_Password, "");
                        ftpResponse = ftpResponse.Replace(" ", "");
                        if (ftpResponse == "221")
                        {
                            var output = await restClient.GetAsync<AttendanceModel>(AppConstants.WRAMS_BaseURl + AppConstants.AttendanceCBranchId + model.CorpBranchLatitudeList[0].BranchId + AppConstants.AttendanceEntityId + model.CorpBranchLatitudeList[0].EntityId +
                               AppConstants.AttendanceDeployementId + model.CorpBranchLatitudeList[0].DeploymentId + AppConstants.AttendanceTime + DateTime.Now.ToString("MM/dd/yyyy HH:mm") + AppConstants.AttendanceDate + DateTime.Now.ToString("MM/dd/yyyy HH:mm") +
                               AppConstants.AttendanceInOutType + type + AppConstants.AttendanceEntityType + AppConstants.AttendanceInOutCapturedImage + fileName, true);
                            if (output != null && output.MessageCode == "1")
                            {
                                TimeType = timetype + " : ";
                                DateType = datetype + " : ";
                                CurrrentTime = DateTime.Now.ToString("hh:mm tt");
                                CurrrentDate = DateTime.Now.ToString("dddd, dd MMMM yyyy");
                                UserImage = ImageSource.FromStream(() => { var stream = _mediaFile.GetStream(); return stream; });
                                RefreshUI();
                                await dialogService.DisplayAlertAsync("Success!!", output.Message, "OK");
                            }
                            else
                                await dialogService.DisplayAlertAsync("Alert!", output.Message, "OK");
                        }
                        else
                            notifier.Notify(ToastNotificationType.Error, "", "Server Error : Image not uploadedbut don't worry we captured for analysis! Thanks.", TimeSpan.FromMilliseconds(2500));
                    }
                }
                else
                    await dialogService.DisplayAlertAsync("Alert!", "Camera Permission Denied. please check permissions and try again", "OK");
            }
            catch (Exception ex)
            {
                notifier.Notify(ToastNotificationType.Error, "", "Something went wrong, but don't worry we captured for analysis! Thanks.", TimeSpan.FromMilliseconds(2500));
                ex.ToString();
            }
        }

        private void RefreshUI()
        {
            RaisePropertyChanged("CurrrentTime");
            RaisePropertyChanged("CurrrentDate");
            RaisePropertyChanged("TimeType");
            RaisePropertyChanged("DateType");
            RaisePropertyChanged("UserImage");
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            model = (AuthModel)parameters["model"];
            if (model != null)
                WelcomeText = "Welcome, " + model.UserList.FirstOrDefault().MWU_FirstName;
            if (model.UserList[0].MWU_Gender.ToLower() == "female")
                UserImage = "ic_female.jpg";
            else
                UserImage = "ic_person.png";
            RefreshUI();
        }

        public async void Logout()
        {
            try
            {
                bool result = await dialogService.DisplayAlertAsync("WRAMS", "Are you sure you want to logout ?", "Yes", "No");
                if (result)
                    NavigationService.NavigateAsync(new Uri("http://www.brianlagunas.com/Navigation/LoginPage", UriKind.Absolute), null, false);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
