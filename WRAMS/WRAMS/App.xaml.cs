﻿using Prism;
using Prism.Ioc;
using Prism.Navigation;
using System;
using WRAMS.Interfaces;
using WRAMS.Services;
using WRAMS.ViewModels;
using WRAMS.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace WRAMS
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        IFtpWebRequest ftpWebRequest;

        protected override async void OnInitialized()
        {
            InitializeComponent();
            ftpWebRequest = Container.Resolve<IFtpWebRequest>();
            NavigationService.NavigateAsync(new Uri("http://www.brianlagunas.com/Navigation/LoginPage", UriKind.Absolute), null, false);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterInstance<IRestClient>(RestClient.Instance);
            containerRegistry.RegisterForNavigation<NavigationPage>("Navigation");
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<AttendancePage, AttendancePageViewModel>();
        }
    }
}
