﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace WRAMS.CustomControls
{
    public class CardView : Frame
    {
        public CardView()
        {
            Padding = 0;
            CornerRadius = 8;
            if (Device.RuntimePlatform == Device.iOS)
            {
                HasShadow = false;
            }
        }
    }
}