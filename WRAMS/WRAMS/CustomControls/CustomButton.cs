﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace WRAMS.CustomControls
{
    public class CustomButton : Button
    {
        public CustomButton() : base()
        {
            const int _animationTime = 250;
            Clicked += async (sender, e) =>
            {
                try
                {
                    var btn = (CustomButton)sender;
                    await btn.ScaleTo(1.2, _animationTime, Easing.Linear);
                    await btn.ScaleTo(1, _animationTime, Easing.Linear);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            };

        }
    }
}
