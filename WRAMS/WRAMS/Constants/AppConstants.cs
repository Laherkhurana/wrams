﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WRAMS.Constants
{
    public class AppConstants
    {
     
        public static string WRAMS_BaseURl = "http://192.168.1.218/MAPI_UAT/api/Values/";
        public static string WRAMS_LiveBaseURl = "http://demo.emtpl.com/MedicalCare_API_LIVE/api/Values/";
        public static string AuthUserId = "Authenticate?UserId=";
        public static string AuthPassword = "&Password=";
        public static string AttendanceCBranchId = "MarkAttendance?CorporateBranchId=";
        public static string AttendanceEntityId = "&EntityId=";
        public static string AttendanceDeployementId = "&DeployementId=";
        public static string AttendanceTime = "&AttendanceTime=";
        public static string AttendanceDate = "&AttendanceDate=";
        public static string AttendanceInOutType = "&InOutType=";
        public static string AttendanceEntityType = "&EntityType=DN";
        public static string AttendanceInOutCapturedImage = "&InOutCapturedImage=";

        #region FTP Detail

        /* 
         FTP_FolderName :-
         For UAT: uatmedicalcare/
         For Live = medicalcare/
        */
        public static string FTP_FolderName = "uatmedicalcare/";
        public static string WRAMS_FTP_URl = "ftp://portal.wellogo.in/" + FTP_FolderName; 
        public static string FTP_DirectoryPath = "upload/AttCapture";
        public static string FTP_UserName = "ftpmedicare"; 
        public static string FTP_Password = "Care@600$ftp";
        
        #endregion

        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }
    }
}
