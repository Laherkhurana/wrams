﻿using Foundation;
using IconEntry.FormsPlugin.iOS;
using Prism;
using Prism.Events;
using Prism.Ioc;
using UIKit;
using WRAMS.Constants;
using WRAMS.Interfaces;
using WRAMS.iOS.Services;

namespace WRAMS.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public static EventAggregator eventAggregator = new EventAggregator();
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            IconEntryRenderer.Init();
            AppConstants.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;
            AppConstants.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;
            LoadApplication(new App(new iOSInitializer()));

            return base.FinishedLaunching(app, options);
        }
        public class iOSInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterInstance<IEventAggregator>(eventAggregator);
                containerRegistry.Register<IToastNotifier, ToastNotifier>();
            }
        }
    }    
}
