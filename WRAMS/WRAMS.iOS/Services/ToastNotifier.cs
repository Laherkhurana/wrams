﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using WRAMS.Interfaces;

namespace WRAMS.iOS.Services
{
    public class ToastNotifier : IToastNotifier
    {
        static MessageBarStyleSheet _styleSheet;

        public static void Init()
        {
            _styleSheet = new MessageBarStyleSheet();
        }

        public void Notify(ToastNotificationType type, string title, string description, TimeSpan duration, object context = null)
        {
            MessageType msgType = MessageType.Info;

            switch (type)
            {
                case ToastNotificationType.Error:
                case ToastNotificationType.Warning:
                    msgType = MessageType.Error;
                    break;

                case ToastNotificationType.Success:
                    msgType = MessageType.Success;
                    break;
            }

            MessageBarManager.SharedInstance.ShowMessage(title, description, msgType);
        }

        public void HideAll()
        {
            MessageBarManager.SharedInstance.HideAll();
        }
    }
}