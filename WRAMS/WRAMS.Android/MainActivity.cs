﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Plugin.Permissions;
using Prism;
using Prism.Events;
using Prism.Ioc;
using WRAMS.Constants;
using WRAMS.Droid.Services;
using WRAMS.Interfaces;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace WRAMS.Droid
{
    [Activity(Label = "WRAMS", Icon = "@drawable/logo", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static EventAggregator eventAggregator = new EventAggregator();
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            AppConstants.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density);
            AppConstants.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density);

            LoadApplication(new App(new AndroidInitializer()));
            App.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        public class AndroidInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterInstance<IEventAggregator>(eventAggregator);
                containerRegistry.Register<IToastNotifier, ToastNotifier>();
                containerRegistry.Register<IFtpWebRequest, FTP>();
            }
        }
    }  
}

