﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WRAMS.Interfaces;
using Xamarin.Forms;

namespace WRAMS.Droid.Services
{
    public class ToastNotifier : IToastNotifier
    {
        public void Notify(ToastNotificationType type, string title, string description, TimeSpan duration, object context = null)
        {
            Toast.MakeText(Forms.Context, description, ToastLength.Short).Show();
        }

        public void HideAll()
        {
        }
    }
}